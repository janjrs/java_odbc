package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataSource {
    private String host;
    private String port;
    private String database;
    private String userName;
    private String passWord;
    private Connection connection;

    public DataSource(){
        try {
            host= "localhost";
            port="3306";
            database="cruddb";
            userName="root";
            passWord="";
            
            String url = "jdbc:mysql://"+host+":"+port+"/"+database;
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            connection = DriverManager.getConnection(url,userName,passWord);
            System.out.println("Deu tudo certo");
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }catch(Exception e){
            System.err.println(e.getMessage());
        }
    }
    public Connection getConnection(){
        return connection;
    }
    
    public void closeConnection(){
        try {
           connection.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
